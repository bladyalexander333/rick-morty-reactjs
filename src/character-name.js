import React from "react";
import styled from 'styled-components'

const CharacterNameStyled = styled.div`
  // /* border: 1px solid blue; */
  display: flex;
  align-items: center;
  grid-area: character-name;
  // /* padding-inline-start: 1em; */
`

function CharacterName({name}) {
    return (
        <CharacterNameStyled>
            <div className="character-name">
                <h2>{name}</h2>
            </div>
        </CharacterNameStyled>
    )
}

export default CharacterName