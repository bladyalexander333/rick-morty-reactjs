import React, {useEffect, useState} from 'react';
import logo from './logo.svg';
import './App.css';
import Character from "./character";
import styled from 'styled-components'
import API from "./api";
import CharacterName from "./character-name";
import CharacterImage from "./character-image";
const api = new API()
const CharacterStyled = styled.div`
`

function App() {
    const [character,setCharacter]= useState({})
    useEffect(()=>{
        async function getCharacter() {
            setCharacter(await api.getCharacter(1))
        }
        getCharacter()
    },[])
    return (
        <div className="App">
            <div className="placeholder-container">
                <h1 id="character-name-placeholder" className="character-name-placeholder"></h1>
            </div>
            <div className="grid">
                <span className="asset top">
                  <span className="dot"></span>
                  <span className="dot"></span>
                  <span className="dot"></span>
                  <span className="line"></span>
                </span>
                <img className="logo" src="/../images/logo@2x.png" width="280" alt=""/>
                <div className="navigation name">
                    <a href="#">Name</a>
                </div>
                <div id="character-name-container" className="character-name-container">
                    <CharacterName name={character.name}/>
                </div>
                <div id="character-image-container" className="character-image-container">
                    <CharacterImage image={character.image} name={character.name}/>
                </div>
                <div className="navigation about">
                    <a href="#">About</a>
                </div>
                <div id="character-description-container" className="character-description-container">
                </div>
                <span className="asset bottom">
                  <span className="line"></span>
                  <span className="dot"></span>
                  <span className="dot"></span>
                  <span className="dot"></span>
                </span>

                <div></div>
                <div className="learn-more">
                    <span>learn more</span>
                </div>

                <div className="arrow" id="load-next"></div>

            </div>
        </div>
    );
}

export default App;
